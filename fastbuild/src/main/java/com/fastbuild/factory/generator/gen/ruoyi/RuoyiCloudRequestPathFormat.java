package com.fastbuild.factory.generator.gen.ruoyi;

import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.domain.ProjectConfig;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;

/**
 * 修改前端请求地址，适配cloud版本
 * @author fastbuild@163.com
 */
public class RuoyiCloudRequestPathFormat {

    private ProjectConfig project;

    private File apiPath;

    public RuoyiCloudRequestPathFormat (ProjectConfig project, File apiPath) {
        this.project = project;
        this.apiPath = apiPath;
    }

    public void format () throws IOException {
        if (FactoryConst.server.CLOUD.equals(this.project.getServerMode())) {
            IOFileFilter fileFilter = FileFilterUtils.asFileFilter(file -> StringUtils.endsWith(file.getName(), "js") && StringUtils.contains(file.getAbsolutePath(), "/src/api/"));
            FileFormatter apiFormatter = new FileFormatter(apiPath, fileFilter);
            apiFormatter.replaceAll("'/login'", "'/auth/login'");
            apiFormatter.replaceAll("'/register'", "'/auth/register'");
            apiFormatter.replaceAll("'/logout'", "'/auth/logout'");
            apiFormatter.replaceAll("'/captchaImage'", "'/code'");
            apiFormatter.replaceAll("'/getRouters'", "'/system/menu/getRouters'");
            apiFormatter.replaceAll("'/getInfo'", "'/system/user/getInfo'");

            apiFormatter.replaceAll("'/monitor/job'", "'/schedule/job'");
            apiFormatter.replaceAll("'/monitor/jobLog'", "'/schedule/job/log'");

            apiFormatter.replaceAll("'/monitor/operlog'", "'/system/operlog'");
            apiFormatter.replaceAll("'/monitor/online'", "'/system/online'");
            apiFormatter.replaceAll("'/monitor/logininfor'", "'/system/logininfor'");

            apiFormatter.format();
        }
    }

}
