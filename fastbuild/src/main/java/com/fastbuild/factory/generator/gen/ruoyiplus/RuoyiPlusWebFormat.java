package com.fastbuild.factory.generator.gen.ruoyiplus;

import com.fastbuild.common.utils.StringUtils;
import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.ruoyi.RuoyiCloudRequestPathFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;

import java.io.File;
import java.io.IOException;

public class RuoyiPlusWebFormat extends AbstractFormat {

    private final String GEN_ID = "ruoyiplus#web";

    public RuoyiPlusWebFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.RUOYI_PLUS.equals(app.getAppId());
    }

    @Override
    protected void dependency() throws Exception {
    }

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = properties.getFactoryRuoyiPlusVuePath() + File.separator + "ruoyi-ui";
        if (StringUtils.isNotEmpty(srcPath)) {
            File srcFile = new File(srcPath);

            String destPath = project.getWorkPath() + File.separator + project.getUiName();
            File destRoot = new File(destPath);

            FileUtils.copyDirectory(srcFile, destRoot, FileFilterUtils.trueFileFilter());

            this.fileContentFormat(destRoot);
        }
    }

    private void fileContentFormat (File destRoot) throws IOException {
        FileFormatter vueFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("vue"));
        vueFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        vueFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        vueFormatter.format();

        FileFormatter developmentFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("development"));
        developmentFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        developmentFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        developmentFormatter.format();

        FileFormatter productionFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("production"));
        productionFormatter.replaceAll("若依管理系统", project.getProjectTitle());
        productionFormatter.replaceAll("若依后台管理系统", project.getProjectTitle());
        productionFormatter.format();

        // 适配微服务的请求地址转换
        RuoyiCloudRequestPathFormat cloudFormat = new RuoyiCloudRequestPathFormat(project, destRoot);
        cloudFormat.format();

    }

}
