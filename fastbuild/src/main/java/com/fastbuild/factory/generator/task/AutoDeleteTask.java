package com.fastbuild.factory.generator.task;

import com.fastbuild.factory.generator.common.FactoryProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Calendar;
import java.util.Date;

/**
 * 自动清除历史缓存文件
 */
@Slf4j
@Component
@EnableAsync
@EnableScheduling
public class AutoDeleteTask {

    @Autowired
    private FactoryProperties properties;

    /**
     * 每天定时删除缓存文件
     */
    @Scheduled(cron = "0 0 3 * * ? ")
    void autoDeleteTask() {

        String pathTemplate = properties.getFactoryTmpRoot();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date date = calendar.getTime();
        String tmpPath = String.format(pathTemplate, date, date, date, "");

        log.info("自动删除任务执行！{}", tmpPath);
        File file = new File(tmpPath);
        if (file.exists()) {
            deleteDir(file);
        }
    }

    public boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        if(dir.delete()) {
            System.out.println("目录已被删除！");
            return true;
        } else {
            System.out.println("目录删除失败！");
            return false;
        }
    }

}
