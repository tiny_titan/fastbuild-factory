package com.fastbuild.factory.generator.gen.ruoyiplus;

import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import com.fastbuild.factory.generator.gen.IGenHandler;

public class RuoyiPlusHandler implements IGenHandler {

    private AppConfig app;

    private AbstractFormat[] genList;

    public RuoyiPlusHandler(AppConfig app) {
        this.app = app;
        this.genList = new AbstractFormat[] {
                new RuoyiPlusServerFormat(this.app),
                new RuoyiPlusWebFormat(this.app),
                new RuoyiPlusMobileFormat(this.app),
        };
    }

    @Override
    public void gen() throws Exception {
        for (AbstractFormat gen : genList) {
            gen.gen();
        }
    }

}
