package com.fastbuild.factory.generator.task;

import lombok.extern.slf4j.Slf4j;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * 自动更新源码仓库
 */
@Slf4j
@Component
public class AutoUpdateTask {

    @Resource
    private ConfigurableEnvironment springEnv;

    public static void updateGit(String workDir) {
        try {
            Repository repository = new FileRepositoryBuilder()
                    .setGitDir(Paths.get(workDir, ".git").toFile())
                    .build();
            Git git = new Git(repository);
            List<Ref> tagRefList = git.tagList().call();
            Collections.reverse(tagRefList);
            git.checkout().setName(tagRefList.get(0).getName()).call();
        } catch (Exception e) {
            log.error("git更新失败:{}", e.getMessage());
        }
    }

    /**
     * 每天零点更新源码仓库
     */
//    @Scheduled(cron = "0 0 0 * * ? ")
    void autoUpdateTask() {
        MutablePropertySources propertySources = springEnv.getPropertySources();

        // 获取所有源码路径配置
        StreamSupport.stream(propertySources.spliterator(), false)
                .filter(ps -> ps instanceof EnumerablePropertySource)
                .map(ps -> ((EnumerablePropertySource<?>) ps).getPropertyNames())
                .flatMap(Arrays::stream)
                .distinct()
                .filter(key -> key.contains("factory") && !key.equals("factory.tmp.root"))
                .collect(Collectors.toMap(Function.identity(), springEnv::getProperty))
                .values().forEach(AutoUpdateTask::updateGit);

    }
}
