package com.fastbuild.factory.generator.gen.ruoyi;

import com.fastbuild.common.utils.StringUtils;
import com.fastbuild.factory.generator.common.FactoryConst;
import com.fastbuild.factory.generator.common.FileFormatter;
import com.fastbuild.factory.generator.common.FileHelper;
import com.fastbuild.factory.generator.domain.AppConfig;
import com.fastbuild.factory.generator.gen.AbstractFormat;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.text.CaseUtils;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 若依单体服务端代码格式化
 *
 * @author fastbuild@163.com
 */
public class RuoyiServerFormat extends AbstractFormat {

    private final String GEN_ID = "ruoyi#server";

    public RuoyiServerFormat(AppConfig app) {
        super(app);
    }

    @Override
    protected String getGenId() {
        return GEN_ID;
    }

    @Override
    protected boolean validate() {
        return FactoryConst.app.RUOYI.equals(app.getAppId());
    }

    @Override
    protected void dependency() {}

    @Override
    protected void fileGenerator() throws Exception {
        String srcPath = this.getSrcPath();
        File srcFile = new File(srcPath);
        File destRoot = new File(project.getServerRootPath());

        List<String> exclude = this.getExcludeFile();
        List<String> excludeSuffix = this.getExcludeFileSuffix();
        List<String> excludeDir = this.getExcludeDir();
        Map<String, String> replaceDirMap = this.getReplaceDirMap();
        Map<String, String> replaceFileMap = this.getReplaceFileMap();

        FileFilter filter = (file) -> {
            if (exclude.contains(file.getName())) {
                return false;
            }
            if (excludeSuffix.contains(StringUtils.substringAfterLast(file.getName(), "."))) {
                return false;
            }
            if (file.isDirectory() && excludeDir.contains(file.getName())){
                return false;
            }
            return true;
        };

        FileHelper.copyDirectory(srcFile, destRoot, filter, replaceDirMap, replaceFileMap);

        this.fileContentFormat(destRoot);
    }

    /**
     * 格式化文件内容
     *
     * @param destRoot
     * @throws IOException
     */
    private void fileContentFormat (File destRoot) throws IOException {
        final String classNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), true, new char[] { '-', '_' });
        final String varNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), false, new char[] { '-', '_' });

        FileFormatter javaFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("java"));
        javaFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        javaFormatter.replaceAll("RuoYi", classNamePrefix);
        javaFormatter.replaceAll("Ruoyi", classNamePrefix);
        javaFormatter.replaceAll("ruoyi", varNamePrefix);
        javaFormatter.replaceAll("若依", project.getProjectTitle());
        javaFormatter.format();

        FileFormatter xmlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("xml"));
        xmlFormatter.replaceAll("http://www.ruoyi.vip", "http://fastbuild.run");
        xmlFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        xmlFormatter.replaceAll("ruoyi", varNamePrefix);
        xmlFormatter.replaceAll("RuoYi", project.getProjectName());
        xmlFormatter.format();

        FileFormatter ymlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("yml"));
        ymlFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        ymlFormatter.replaceAll("RuoYi", project.getProjectName());
        ymlFormatter.replaceAll("ruoyi", varNamePrefix);
        ymlFormatter.format();

        FileFormatter propertiesFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("properties"));
        propertiesFormatter.replaceAll("RuoYi", project.getProjectName());
        propertiesFormatter.replaceAll("ruoyi", project.getProjectName());
        propertiesFormatter.format();

        FileFormatter vmFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("vm"));
        vmFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        vmFormatter.replaceAll("ruoyi", varNamePrefix);
        vmFormatter.format();

        FileFormatter importsFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("imports"));
        importsFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        importsFormatter.replaceAll("ruoyi", varNamePrefix);
        importsFormatter.format();

        FileFormatter sqlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("sql"));
        sqlFormatter.replaceAll("ruoyi", project.getProjectName());
        sqlFormatter.format();

        FileFormatter confFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("conf"));
        confFormatter.replaceAll("ruoyi", project.getProjectName());
        confFormatter.format();

        FileFormatter shFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("sh"));
        shFormatter.replaceAll("ruoyi", project.getProjectName());
        shFormatter.format();

        FileFormatter txtFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("txt"));
        txtFormatter.replaceAll("ruoyi", project.getProjectName());
        txtFormatter.format();

        FileFormatter batFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("bat"));
        batFormatter.replaceAll("ruoyi", project.getProjectName());
        batFormatter.format();

        FileFormatter dockerfileFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("dockerfile"));
        dockerfileFormatter.replaceAll("ruoyi", project.getProjectName());
        dockerfileFormatter.format();

        FileFormatter factoriesFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("factories"));
        factoriesFormatter.replaceAll("com.ruoyi", project.getPackagePrefix());
        factoriesFormatter.replaceAll("RuoYi", classNamePrefix);
        factoriesFormatter.replaceAll("ruoyi", classNamePrefix);
        factoriesFormatter.format();

        if (FactoryConst.web.THYMELEAF.equals(this.project.getWebFramework())) {
            FileFormatter htmlFormatter = new FileFormatter(destRoot, FileFilterUtils.suffixFileFilter("html"));
            htmlFormatter.replaceAll("ruoyi", project.getProjectName());
            htmlFormatter.format();
        }
    }

    private String getSrcPath () {
        if (FactoryConst.server.CLOUD.equals(this.project.getServerMode())) {
            if (FactoryConst.db.ORACLE.equals(this.project.getDatabase())) {
                return properties.getFactoryRuoyiCloudOraclePath();
            } else {
                return properties.getFactoryRuoyiCloudPath();
            }
        } else if (FactoryConst.web.THYMELEAF.equals(this.project.getWebFramework())) {
            return properties.getFactoryRuoyiFastPath();
        } else if (FactoryConst.db.MYSQL.equals(this.project.getDatabase())) {
            return properties.getFactoryRuoyiVuePath();
        } else if (FactoryConst.db.ORACLE.equals(this.project.getDatabase())) {
            return properties.getFactoryRuoyiOraclePath();
        } else if (FactoryConst.db.SQL_SERVER.equals(this.project.getDatabase())) {
            return properties.getFactoryRuoyiSqlServerPath();
        }
        return null;
    }

    private List<String> getExcludeFile () {
        List<String> exclude = new ArrayList<>();
        exclude.add(".git");
        exclude.add(".github");
        exclude.add(".idea");
        exclude.add("ruoyi-ui");
        exclude.add("README.md");
        exclude.add("v3");
        exclude.add("target");
        exclude.add(".DS_Store");
        exclude.add(".jar");
        return exclude;
    }

    private List<String> getExcludeFileSuffix() {
        List<String> exclude = new ArrayList<>();
        exclude.add("jar");
        exclude.add("log");
        return exclude;
    }

    private List<String> getExcludeDir() {
        List<String> exclude = new ArrayList<>();
        exclude.add("logs");
        exclude.add("data");
        return exclude;
    }

    private Map<String, String> getReplaceDirMap () {
        Map<String, String> replaceDirMap = new LinkedHashMap<>();
        replaceDirMap.put("ruoyi-", project.getProjectName() + "-");
        replaceDirMap.put("src/main/java/com/ruoyi", "src/main/java/" + project.getPackagePrefix().replaceAll("\\.", "/"));
        replaceDirMap.put("docker/ruoyi", "docker/" + project.getProjectName());

        if (FactoryConst.web.THYMELEAF.equals(this.project.getWebFramework())) {
            replaceDirMap.put("resources/static/ruoyi", "resources/static/" + project.getProjectName());
        }
        return replaceDirMap;
    }

    private Map<String, String> getReplaceFileMap () {
        final String classNamePrefix = CaseUtils.toCamelCase(project.getProjectName(), true, new char[] { '-', '_' });
        Map<String, String> replaceFileMap = new LinkedHashMap<>();
        replaceFileMap.put("Ruoyi", classNamePrefix);
        replaceFileMap.put("RuoYi", classNamePrefix);
        replaceFileMap.put("ruoyi", project.getProjectName());
        return replaceFileMap;
    }

}
